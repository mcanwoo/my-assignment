from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from myapi.models import NavigationRecord
from myapi.models import Bin
from datetime import datetime, timedelta

# Create your views here.
def getLatestPoints(request):
    time_threshold = datetime.now() - timedelta(hours=48)
    data = list(NavigationRecord.objects.filter(datetime__gte=time_threshold).values().order_by('vehicle_id', '-datetime').distinct('vehicle_id')) 
    return JsonResponse(data, safe=False)
    
def getFrequency(request):
    data = list(Bin.objects.values()) 
    return JsonResponse(data, safe=False)
   