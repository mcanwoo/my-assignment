from django.db import models

class Vehicle(models.Model):
    plate = models.CharField(max_length=10)
    
class NavigationRecord(models.Model):
    vehicle = models.ForeignKey(Vehicle, on_delete=models.DO_NOTHING)
    datetime = models.DateTimeField(auto_now_add=True, blank=True)
    latitude = models.FloatField()
    longitude = models.FloatField()

class Bin(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    collection_frequency = models.FloatField()
class Operation_type(models.Model):
    name = models.CharField(max_length=50)

class Operation(models.Model):
    type = models.ForeignKey(Operation_type, on_delete=models.DO_NOTHING)
    bin_id = models.ForeignKey(Bin, on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)