﻿# Q1

- I wrote an ORM query to collect latest point information
```python
def getLatestPoints(request):
    time_threshold = datetime.now() - timedelta(hours=48)
    data = list(NavigationRecord.objects.filter(datetime__gte=time_threshold).values().order_by('vehicle_id', '-datetime').distinct('vehicle_id')) 
    return JsonResponse(data, safe=False)
```
- The whole code is taking 1 SQL query into DB. More efficient than Solutions with foreach or iterative functions.
# Q2

##### I Redesigned the Tables.


![alt text](https://gitlab.com/mcanwoo/my-assignment/-/raw/master/Assignment%20(1).png "Diagram")
- I added bin_id (Foreign Key) to Operation table so every bin now can have multiple Operations
- Removed "name" column from Operation table and pointed it to another table so operations table will be tinier when there's many records.
- created_at column will replace last_collection field. It will be gathered from Operations table(Latest Record)
- Kept collection_frequency to prevent unnecessary CPU usage on calculating frequency on every call. It will be calculated when new records added in Operation table (with DB Triggers) and will be cached in collection_frequency column.

